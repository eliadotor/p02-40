package herencia;

public class TestVehiculo {
	public static void main(String[] args) {
		Vehiculo miVehiculo = new Vehiculo(4, "diesel", "ford", "rojo", "tdi1.5");
		System.out.println(miVehiculo.mostrarDatos());
		Camion miCamion = new Camion(4, "diesel", "volvo", "blanco", "tdi1500", 10000);
		System.out.println(miCamion.mostrarDatos());
		
	}

}
