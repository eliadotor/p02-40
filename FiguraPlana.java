package herencia;
/**
 * Figura plana es una clase abstracta   es aquella que tiene ppor lo menos un metodo abstracto 
 * Un metodo abstracto es una herramienta que tiene java para crear polimosfirmo.
 * No se pueden crear objetos de una clase abstracta
 *
 *
 */

public abstract class FiguraPlana 
{
	//Atributos
	
	protected double base;
	protected double altura;
	public FiguraPlana(double base, double altura) 
	{
		super();
		this.base = base;
		this.altura = altura;
	}
///////////////////////////////////////////////////////////////////////////////////////
	public double getBase() 
	{
		return base;
	}
///////////////////////////////////////////////////////////////////////////////////////

	public void setBase(double base) 
	{
		this.base = base;
	}
///////////////////////////////////////////////////////////////////////////////////////

	public double getAltura() 
	{
		return altura;
	}
///////////////////////////////////////////////////////////////////////////////////////

	public void setAltura(double altura) 
	{
		this.altura = altura;
	}
///////////////////////////////////////////////////////////////////////////////////////
	public String mostrarDatos()
	{
		return base + "\n" + altura;
	}
///////////////////////////////////////////////////////////////////////////////////////
	//metodo abstracto
	public abstract double area();
	public abstract int perimetro();

	

}
