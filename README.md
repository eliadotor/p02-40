#Práctica P02-40

**Elia Dotor Puente y Jorge Sánchez-Flor Sánchez**

Primero me creo un repositorio en *Bitbucket*

Clono mi repositorio de *bitbucket* en eclipse

![Clono repositorio](imagenes/clonoRepositorio.png)

A continuación creo un paquete en eclipse llamado *P02-40* y dentro importo algunos ficheros de java  
Añado un nuevo fichero *README.md* donde documentaremos toda la práctica

En el fichero *.gitignore* incluimos lo que no queremos subir a nuestro repositorio remoto

![.gitignore](imagenes/gitignore.png)

Una vez hecho esto guardo los cambios y compruebo que se encuentran en *unstaged* (Sería como hacer un `git status` en la consola)

Y posteriormente los añado a *Staged* (Sería como hacer un `git add` en la consola)
![add](imagenes/staged.png)


Haremos un `commit and push` para subir nuestro proyecto a nuestro repositorio remoto
![Commit and push](imagenes/push2.png)


Posteriormente haremos cambios cada uno en un fichero  
Después de modificar mi fichero lo guardo y estará en *unstaged*
Después lo añado a *staged* y hago un *commit* "Cambios Elia"

Cuando Jorge  intenta subir sus cambios le da un error.  
![Fetch_problem_ocurred](imagenes/error_rama.png)

Decimos entonces crear una rama nueva *desarrollojorge*:

![configure_fetch](imagenes/configureFetch.png)  
![configure_fetch](imagenes/fetch2.png)

Jorge realiza sus cambios en esa rama

Al finalizar fusiona su rama con la rama master  
![configure_fetch](imagenes/fetch_results.png)

También creamos la rama *desarrolloelia* para poder trabajar ambos paralelamente sin colisionar
![Crear rama desarrolloelia](imagenes/desarrolloelia.png)


Subo mi última versión con cambios fichero *figuraplana.java*

Subo la carpeta imagenes para poder documentar la práctica
![Carpeta imagenes](imagenes/imagenes.png)

Por último fusiono la rama desarrolloelia con la rama master con un `merge`

![Merge desarrolloelia](imagenes/merge.png)











